<?php
//介绍使用trait
//php面向对象中extends只能继承一个类[单继承]
//trait(与"类"相似)的出现是实现php"变形多继承"的机制

//传统面向对象不能多继承
//class AA{}
//class CC{}
//class DD{}
//class Person  extends AA,CC,DD{}   //报错，不能多继承

//通过trait实现"多继承"效果
trait AA{function saya(){echo "I am aa";}}
trait CC{function sayc(){echo "I am cc";}}
trait DD{function sayd(){echo "I am dd";}}
class Person{
    //表示当前类可以使用AA,CC,DD中的成员
    use AA,CC,DD;
}

$tom = new Person();
$tom -> saya();     //I am aa
$tom -> sayc();     //I am cc
$tom -> sayd();     //I am dd
