<?php

//命名空间设置语法可以参考控制器(模型文件名字的上级目录)
namespace app\admin\model;

/*
 * 给model模型类起名字[大驼峰命名风格]，数据表名字的每个单词的第一个字母大写
 * 数据表sp_goods------>模型model名字Goods
 * 数据表sp_user------>模型model名字User
 * 数据表sp_userinfo------>模型model名字Userinfo
 * 数据表sp_user_info------>模型model名字UserInfo
 * 数据表sp_user_info_opp------>模型model名字UserInfoOpp
 *
 * model模型类的名字后期也会用于与自己的“数据表”对应
 */

//每个model模型类都需要继承父类，这样本身才可以调用许多成员
//父类：D:\www\php70\shop\thinkphp\library\think\Model.php

//引入软删除机制对应的SoftDelete元素
use traits\model\SoftDelete;

class Goods extends \think\Model
{
    use SoftDelete;
}















