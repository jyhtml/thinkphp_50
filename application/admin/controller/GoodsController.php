<?php

//命名空间
namespace app\admin\controller;

//必须在class外部引入
use app\admin\model\Goods;
//引入Request请求类
use think\Request;

//声明类
class GoodsController extends \think\Controller
{
    /**
     * 商品列表展示
     */
    public function showlist(Request $request)
    {
        dump($request->domain());  //获得当前请求的"域名"信息
        dump($request->module());  //获得当前请求的"分组"信息
        dump($request->controller());  //获得当前请求的"控制器"信息
        dump($request->action());  //获得当前请求的"操作方法"信息


        //获得被展示的商品信息
        $infos = Goods::order('goods_price desc')->select();
        //$infos是对象结果集
        //dump($infos);[obj,obj,obj,obj]

        //把$infos传递给模板
        //$this -> assign(变量在模板中使用的名称,被传递变量的值);
        $this -> assign('infos',$infos);

        return $this -> fetch();
    }

    /**
     * 删除商品
     */
    public function del(Request $request)
    {
        if($request->isPost()){
            //实现商品商品
            //被删除商品的id是通过post方式提交过来的
            $goods_id = $request->post('goods_id');

            $num = Goods::destroy($goods_id);
            if($num==1){
                return ['status'=>'success'];
            }else{
                return ['status'=>'failure'];
            }
        }
    }

    /**
     * @return mixed
     * 修改商品
     * 当前方法对应路由格式：http://网站/admin/goods/upd/goods_id/xxxx
     * 承接两种请求：get/post
     */
    //public function upd(Request $request,$goods_name,$goods_id)
    public function upd(Request $request,$goods_id)
    {
        if($request->isPost()){
            //收集数据存储入库
            //$shuju = $request -> post();
            //通过only方法把指定的信息从post表单中赛选出来
            $shuju = $request -> only('goods_name,goods_price,goods_number,goods_weight,goods_introduce');

            //dump($shuju);  //一维数组信息
            $goods = new Goods();
            $num = $goods ->where('goods_id',$shuju['goods_id'])-> update($shuju);
            if($num==1){
                return ['status'=>'success'];
            }else{
                return ['status'=>'failure'];
            }
        }
        //获得被修改商品的信息
        $info = Goods::find($goods_id);  //是一个包含查询记录的model模型对象
        //dump($info);
        $this -> assign('info',$info);

        return $this -> fetch();
    }


    /**
     * 添加商品
     * @return mixed
     * @request  请求类对象
     * Request：D:\www\php70\shop\thinkphp\library\think\Request.php
     */
    public function add(Request $request)
    {
        //判断当前请求类型get、post
        if($request->isPost()){
            //收集客户端提交过来的信息并直接存储即可
            //tp框架中推荐我们使用$request对象去获得客户端提交过来的信息
            //$request->post(); //收集全部的post表单域信息,与$_POST效果一致
            //$request->get();    //收集全部的浏览器get方式提交的信息,与$_GET效果一致
            $shuju = $request->post();

            $goods = new Goods();
            $rst = $goods ->allowField(true)->save($shuju);
            if($rst==1){
                return ['status'=>'success'];    //tp框架内部会自动把"数组"变为"json"格式 json_encode
            }else{
                return ['status'=>'failure'];    //tp框架内部会自动把"数组"变为"json"格式
            }
        }
        return $this -> fetch();
    }



    /**
     * 商品列表展示
     */
//    public function showlist()
//    {
//        //使用Goods模型
////        $obj = new \app\admin\model\Goods();
//        $obj = new Goods();
//        dump($obj);    //是tp框架封装好按照一定格式输出内容的函数
//                        //该函数位置：thinkphp/helper.php
//
//        return $this->fetch();
//    }

    /**
     * 商品列表展示
     */
    public function showlistA()
    {

        $infos = \think\Db::query('select * from sp_goods');
        dump($infos);

        //$rst = \think\Db::execute('insert into sp_goods (goods_name) values("白梅手机")');
        //echo "写入记录条数为：".$rst;
        //exit;
        //展示各种连贯方法使用
        $goods = new Goods();
        //① field限制查询字段
        Goods::field('goods_name,goods_price')-> select();
        //select goods_name,goods_price from sp_goods
        $goods  -> field('goods_name')
                ->where('goods_price','>',100)
                ->select();
        //select goods_name from sp_goods where goods_price >100

        //② limit限制查询条数
        $goods -> limit(3)->select();
        //select * from sp_goods limit 3;
        $goods -> limit(3,5)->select();
        //select * from sp_goods limit 3,5; //从第4条记录往后获取5条出来

        //③ order排序查询
        $goods -> order('goods_price desc')->select();
        //select * from sp_goods order by goods_price desc
        $goods -> order('goods_price desc,goods_number asc')->select();
        //select * from sp_goods order by goods_price desc,goods_number asc


        //多个条件方法可以连贯使用(各个方法前后没有顺序要求)
        $goods  ->where('goods_price','>',100)
                ->limit(5)
                ->field('goods_name')
                ->order('goods_price desc')
                ->select();
        //select goods_name from sp_goods where goods_price>100 order by goods_price desc limit 5

        //使用where(字段,比较运算符= >  >=  <=  < in between等,值)方法给sql语句设置执行条件
//        $goods = new Goods();
//        $goods -> where('goods_price','>',1000)->select();
//        //select * from sp_goods where goods_price>1000
//        $goods -> where('goods_name','=','黑莓手机1541578437')->select();
//        $goods -> where('goods_name','黑莓手机1541578437')->select();
//        //select * from sp_goods where goods_name='黑莓手机1541578437'
//        $goods -> where('goods_id','in','7,8,9')->select();
//        //select * from sp_goods where goods_id in (7,8,9)
//
//        //where()方法可以调用多次
//        $goods -> where('goods_price','>',200)
//               -> where('goods_number','<',300)
//               -> select();
//        //select * from sp_goods where goods_price>200 and goods_number<300


        //做数据查询操作
        /*
        //1) 查询一条记录信息
        //语法：模型类::find(主键id值);
        //示例：$info = Goods::find(7);   //获得主键id=7的记录信息
        //$info代表获得到的记录信息，并且是通过model模型对象形式体现，该对象中集成到了被查询记录的内容
        $info = Goods::find(7);
        //dump($info);

        //被查询到记录信息展示
        echo "名字：".$info->goods_name."<br />";
        echo "价格：".$info->goods_price."<br />";
        echo "数量：".$info->goods_number."<br />";
        */

        //2) 获得多条记录信息
        //语法：
        //模型类::select()             //获得数据表全部记录信息
        //模型类::select('7,8,19')     //获得主键id值在(7,8,19)范围内的多条记录
        //示例：$infos = Goods::select();
        //$infos返回值：是一个对象结果集，形式为[obj,obj,obj,obj...]

//        $infos = Goods::select();
//        //dump($infos);
//
//        //遍历输出获得到的多条记录信息
//        foreach($infos as $info){
//            echo "主键id：".$info->goods_id.'--名称：'.$info->goods_name.'--价格：'.$info->goods_price."<br />";
//        }

        //exit;  //打断点，后续代码停止执行(调试工具也就不执行了)

        return $this->fetch();
    }


    /**
     * 商品列表展示
     */
    public function showlistB()
    {
        //获得被展示的商品信息
        $infos = Goods::order('goods_price desc')->select();
        //$infos是对象结果集 [obj,obj,obj,obj]

        //把$infos传递给模板
        //$this -> assign(变量在模板中使用的名称,被传递变量的值);
        $this -> assign('infos',$infos);

        //给模板传递"数组"
        $city = ['b'=>'北京','t'=>'天津','g'=>'广州'];
        $this -> assign('city',$city);

        //给模板传递"对象"
        //find();
        $info = Goods::find(21);
        //dump($info);    //object(app\admin\model\Goods)#33
        $this -> assign('info',$info);


        $weather = "sunshine";
        $addr = "shunyi";
        //1) 通过assign()方法给模板传递变量
        $this -> assign('weather',$weather);
        $this -> assign('addr',$addr);

        //2) 通过fetch方法给模板传递变量
        //return $this ->fetch('',数组)  //数组就是给模板传递的变量信息
        //                               在模板中使用的变量名称就是数组的"键"
        //                               访问到的信息就是数组的"值"
        return $this -> fetch('',['w'=>$weather,'a'=>$addr]);
    }


    
    /**
     * 添加商品
     */
    public function addA()
    {
        //利用model模型类对象实现数据表记录增加操作
        //tp框架关于数据库操作都封装为方法的实现
        //insert into xxxx

        //1)实例化对象
        $obj = new Goods();

        //2) 把被添加的数据通过"数组"形式组织好，一维关联数组，元素下标就是数组表"字段"名称
        $shuju = [
            'goods_name'=>'黑莓手机'.time(),
            'goods_price'=>3400.56,
            'goods_num'=>120,
        ];

        //3) 对象调用方法实现数据添加
        //   save()方法会返回添加数据的记录条数
        $num = $obj ->allowField(true)-> save($shuju);

        //dump($obj);
        //echo '记录已经添加成功，被添加记录条数为：'.$num;
        //立即获得新记录的主键id值
        echo "当前被添加记录的主键id值信息为：".$obj->goods_id;

        return $this -> fetch();
    }
    
    
    /**
     * 修改商品
     */
    public function updA()
    {
        //修改数据，把goods_id=15记录的数量goods_number字段改为199

        //①实例化对象
        $goods = new Goods();

        //② 设置修改数据,就是一个数组，"键"为数据表字段信息，"值"就是修改数据
        $shuju = [
            //'goods_id' => 15,  //A.在数据中设置主键id值条件，update()方法返回model对象
            'goods_number'=>207,
        ];

        //③对象调用update()方法实现数据修改
        //  B.调用where()方法，具体是在被执行的sql语句中设置where条件，返回受影响记录条数
        $num = $goods ->where('goods_id',15)-> update($shuju);
        //$obj = $goods -> update($shuju);  //返回当前模型对象

        echo "当前被修改记录数目为：".$num;


        //展示模板，如果没有return关键字，页面是白板
        return $this -> fetch();
    }



}









