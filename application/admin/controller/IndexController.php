<?php

//命名空间
namespace app\admin\controller;

//声明类
class IndexController extends \think\Controller
{
    /**
     * 后台首页
     */
    public function index()
    {
        return $this -> fetch();
    }
    
    
    /**
     * 首页右下角的部分
     */
    public function welcome()
    {
        return $this -> fetch();
    }
}

