<?php
namespace app\index\controller;

/**
 * Class IndexController
 * @package app\index\controller
 * 默认控制器
 */
//父类：Controller
//D:\www\php70\shop\thinkphp\library\think\Controller.php

//在当前类中使用Controller有两种方式：
//1) 使用  命名空间类元素引入
//use think\Controller;
//class IndexController extends Controller

//2）完全限定名称方式(形象上看 就是通过绝对路径方式引入的Controller)
class IndexController extends \think\Controller
{
    /**
     * @return string
     * 默认被请求的方法
     * 后期应用在网站首页中
     */
    public function index()
    {
        //调用模板，通过视图模板展示具体内容
        //$this：代表当前控制器类的对象
        //fetch()方法存在于当前控制器的"父类Controller"中
        return $this -> fetch();
    }

    public function hello()
    {
        return $this -> fetch();
    }
}
