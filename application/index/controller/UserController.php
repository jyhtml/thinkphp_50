<?php
namespace app\index\controller;
//该命名空间信息  与 该控制器文件的上级目录保持一致(命名空间名字信息 与 目录名字 没有一致要求)
//tp框架中 为什么让命名空间 与 文件目录名字保存一致
//答：该控制器文件在寻找的过程中，命名空间 会 变为目录的一部分，进而帮助寻找该文件
//app代表application
/**
 * Class UserController
 * 会员控制器
 * 类名字 与 该类文件名字 保持一致
 */
class UserController extends \think\Controller
{
    /**
     * 登录的方法
     * 在浏览器中访问login
     * http://tp.shop.com/index.php/index/user/login
     */
    public function login()
    {
        //1) 调用模板文件名称  与  当前操作方法名称 一致
        return $this -> fetch();

        //2) 调用当前控制器标志 下的其他模板文件(register.html)
        //return $this -> fetch('register');

        //3) 调用其他控制器标志 下的showlist.html模板文件
        //return $this -> fetch('goods/showlist');

        //4) 通过模板文件"绝对路径名"方式调用模板
        //return $this -> fetch('D:\www\php70\shop\application\index\view\goods\detail.html');
    }

    /**
     * 注册方法
     */
    public function register()
    {
//        return '注册系统ing';
        return $this -> fetch();
    }
}

