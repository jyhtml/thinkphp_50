<?php

/*
 *
 * 给当前项目的所有路由做备案记录，使得可以被访问
 * 做路由注册的功能文件：D:\www\php70\shop\thinkphp\library\think\Route.php
 * 因此在当前文件中要使用Route类元素
 * 具体有两种方式
 * 1) 命名空间类元素引入方式
 * use think\Route;
 * Route::xxx();
 * 2) 完全限定名称方式
 * \think\Route::xx();
 * 当前文件如果对Route使用较频繁，就用1)方式，否则用2)方式
*/
use think\Route;

//Route::rule(使用的路由地址,  实际执行的地址, 请求方式get/post);
//Route::rule('login',  'index/user/login', 'get');
//浏览器访问：http://网址/login
//实际执行路由：http://网址/index/user/login

//上述login注册路由也可以变为如下效果
//Route::get('login','index/user/login');
//为了减少学习成本，前期设置的 请求地址 与 实际执行地址 请保持一致，即如下的具体路由信息
//Route::get('index/user/login','index/user/login');

//【index】分组路由注册
//商品相关
Route::get('index/goods/showlist','index/goods/showlist');
Route::get('index/goods/detail','index/goods/detail');
//会员相关
Route::get('index/user/login','index/user/login');
Route::get('index/user/register','index/user/register');
//首页
Route::get('/','index/index/index');

//【admin】分组路由注册
//管理员
Route::get('admin/manager/login','admin/manager/login');
//受页面
Route::get('admin/index/index','admin/index/index');
Route::get('admin/index/welcome','admin/index/welcome');
//商品相关
Route::get('admin/goods/showlist','admin/goods/showlist');
//添加商品get/post
//旧的路由设置
//Route::get('admin/goods/add','admin/goods/add');
//Route::post('admin/goods/add','admin/goods/add');
// 新的路由设置 检测路由规则仅GET和POST请求有效
Route::any('admin/goods/add','admin/goods/add',['method'=>'GET|POST']);

//修改商品get/post
Route::any('admin/goods/upd','admin/goods/upd',['method'=>'GET|POST']);

//删除商品
Route::post('admin/goods/del','admin/goods/del');


































